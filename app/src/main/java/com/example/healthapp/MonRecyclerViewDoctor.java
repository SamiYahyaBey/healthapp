package com.example.healthapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MonRecyclerViewDoctor extends RecyclerView.Adapter<MonRecyclerViewDoctor.ConteneurDeDonnee> {


    private ArrayList<DonneeDoctor> donneeDoctors;


    public MonRecyclerViewDoctor(ArrayList<DonneeDoctor> donneeDoctors) {
        this.donneeDoctors = donneeDoctors;
    }


    @Override
    public ConteneurDeDonnee onCreateViewHolder(ViewGroup parent, int
            viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doctors, parent, false);
        return new ConteneurDeDonnee(view);
    }

    @Override
    public void onBindViewHolder(ConteneurDeDonnee conteneur, int
            position) {
        conteneur.tv_principal.setText(donneeDoctors.get(position).getPrincipal());
       // conteneur.tv_auxiliaire.setText(donneeDoctors.get(position).getAuxiliaire());
        conteneur.imageView.setImageDrawable(donneeDoctors.get(position).getimage());

    }
    @Override
    public int getItemCount() {
        return donneeDoctors.size();
    }

    public static class ConteneurDeDonnee extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_principal;
      //  TextView tv_auxiliaire;
        ImageView imageView;
        public ConteneurDeDonnee(View itemView) {
            super(itemView);
            tv_principal = (TextView) itemView.findViewById(R.id.activity_name);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener((View.OnClickListener) this);
        }


        @Override
        public void onClick(View v) {

        }
    }


}