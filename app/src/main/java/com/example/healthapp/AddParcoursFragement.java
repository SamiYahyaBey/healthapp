package com.example.healthapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class AddParcoursFragement extends DialogFragment {


    TextView titre_parcours, description_parcours, description_category;
    Button ajout_parcours;
    private SimpleDialogListener listener;
    FirebaseFirestore Fstore;
    String parcoursID;
    //outil de la base de données
    DatabaseReference parcoursDataBaseReference;



    public static AddParcoursFragement newInstance(String title) {

        AddParcoursFragement apf = new AddParcoursFragement();
        Bundle args = new Bundle();
        args.putString("title", title);
        apf.setArguments(args);
        return apf;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,

                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragement_parcours, container);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        listener = (SimpleDialogListener) getActivity();

        titre_parcours = (EditText) view.findViewById(R.id.parcours_title);
        description_parcours = (EditText) view.findViewById(R.id.parcours_description);
        description_category = (EditText) view.findViewById(R.id.parcours_cateogry);
        Fstore = FirebaseFirestore.getInstance();
        ajout_parcours= (Button) view.findViewById(R.id.add_parcours);
        //instanication de la base

        parcoursDataBaseReference = FirebaseDatabase.getInstance("https://healthapp-bfa8d-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("parcours");

        // quand le button est cliqué, l'activité est appellé,
        // la valeur mEditText est passeé à l'activité en paramètre

        ajout_parcours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               insertParcours();
            }
        });

        String title = getArguments().getString("title", "Votre nom");

        getDialog().setTitle(title);

        titre_parcours.requestFocus();

        getDialog().getWindow().setSoftInputMode(

                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }

    void insertParcours(){
        String tp = titre_parcours.getText().toString().trim();
        String dp = description_parcours.getText().toString().trim();
        String dc = description_category.getText().toString().trim();

        Parcours parcours = new Parcours(tp, dp, dc);

        parcoursDataBaseReference.push().setValue(parcours);
    }


}
