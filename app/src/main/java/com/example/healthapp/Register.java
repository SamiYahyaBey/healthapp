package com.example.healthapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {

    EditText Fnom, Fprenom, Femail, Fpasswod, Fnum_identification;
    TextView fnum;
    Button Bconfirm;
    ProgressBar pb;
    FirebaseAuth fauth;
    FirebaseFirestore Fstore;
    //checkboxes
    CheckBox isDoctor, isIntervenant, isPatient;
    TextView login;
    String userID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Fnom = (EditText)findViewById(R.id.parcours_title);
        fnum = (TextView)findViewById(R.id.fnum_text);
        Fprenom = (EditText)findViewById(R.id.parcours_description);
        Femail= (EditText)findViewById(R.id.parcours_cateogry);
        Fpasswod = (EditText)findViewById(R.id.PasswordRegister);
        Fnum_identification = (EditText)findViewById(R.id.num_id);
        Bconfirm = findViewById(R.id.add_parcours);
        pb = findViewById(R.id.progressBar);
        login = (TextView) findViewById(R.id.loginLink);
        String role ="doctor";
        //Checkboxes
        isDoctor = findViewById(R.id.checkBoxDoctor);
        isIntervenant = findViewById(R.id.checkBoxIntervenant);
        isPatient = findViewById(R.id.checkBoxPatient);

        fauth = FirebaseAuth.getInstance();
        Fstore = FirebaseFirestore.getInstance();

        //controle que les checkboxes ne sont pas cliquées en même temps

        isDoctor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    isIntervenant.setChecked(false);
                    isPatient.setChecked(false);
                    Fnum_identification.setVisibility(View.VISIBLE);
                    fnum.setVisibility(View.VISIBLE);
                }
            }
        });

        isIntervenant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    isDoctor.setChecked(false);
                    isPatient.setChecked(false);
                    Fnum_identification.setVisibility(View.VISIBLE);
                    fnum.setVisibility(View.VISIBLE);

                }
            }
        });

        isPatient.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    isIntervenant.setChecked(false);
                    isIntervenant.setChecked(false);
                    Fnum_identification.setVisibility(View.INVISIBLE);
                    fnum.setVisibility(View.INVISIBLE);
                }
            }
        });

        if(!isDoctor.isChecked() || !isIntervenant.isChecked()||!isPatient.isChecked()){
            Toast.makeText(Register.this, "Vous devez renseigner votre statut",Toast.LENGTH_SHORT).show();
        }



        Bconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                String email = Femail.getText().toString().trim();
                String password = Fpasswod.getText().toString().trim();
                String nom = Fprenom.getText().toString().trim();
                String prenom = Fnom.getText().toString().trim();
                String numero_identification = Fnum_identification.getText().toString().trim();
                String role = "doctor";

                if(password.length()<=5){
                  //  Fpasswod.setError("Le mot de passe est trop court");
                    Toast.makeText(Register.this, "Le mot de passe est trop court",Toast.LENGTH_SHORT).show();
                }



                //insérer les id de l'user dans la bdd
                fauth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull  Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(Register.this, "Inscription réussie",Toast.LENGTH_SHORT).show();
                                //id qui réucpère l'id du doctor
                                userID = fauth.getCurrentUser().getUid();
                                //document ou sera stockée la collection
                                DocumentReference dr = Fstore.collection("doctors").document(userID);
                                //création collection et ajout des objets
                                Map<String, Object> doctor = new HashMap<>();
                                doctor.put("email",email);
                                doctor.put("nom",nom);
                                doctor.put("prenom",prenom);
                                doctor.put("numero_identification",numero_identification);
                               // doctor.put("role", role);
                              //  Insertion du role par rapport à la chackbox
                                if(isDoctor.isChecked()){
                                    doctor.put("isDoctor", "1");
                                }
                                if(isIntervenant.isChecked()){
                                    doctor.put("isIntervenant", "1");
                                }
                                if(isPatient.isChecked()){
                                    doctor.put("isPatient", "1");
                                }
                                dr.set(doctor).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(Register.this, "User created" + userID,Toast.LENGTH_SHORT).show();
                                    }
                                });
                                startActivity(new Intent(getApplicationContext(),Login.class));


                            }else{
                                Toast.makeText(Register.this, "Erreur",Toast.LENGTH_SHORT).show();
                            }
                    }
                });

            }
        });



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Login.class));
            }
        });

    }
}