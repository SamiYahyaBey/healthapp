package com.example.healthapp;

public interface SimpleDialogListener {
    void onOkClickDialog(String inputText);
}
