package com.example.healthapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class Login extends AppCompatActivity {

    EditText Femail, Fpasswod;
    Button Bconnexion;
    ProgressBar pb;
    FirebaseAuth fauth;
    TextView resetPassword, register;
    FirebaseFirestore Fstore;
    //String userID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Femail = findViewById(R.id.emailLogin);
        Fpasswod = findViewById(R.id.passwordLogin);
        fauth = FirebaseAuth.getInstance();
        Fstore = FirebaseFirestore.getInstance();
        Bconnexion = findViewById(R.id.connexion);
        resetPassword = (TextView)findViewById(R.id.forgotPassword);
        register =  (TextView)findViewById(R.id.registerLink);


        Bconnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = Femail.getText().toString().trim();
                String password = Fpasswod.getText().toString().trim();


                //authentification de l'utilisateur

/*                fauth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                       public void onComplete(@NonNull Task<AuthResult> task) {
                           // DocumentReference dr = Fstore.collection("doctors").document(userID);
                            if(task.isSuccessful()){

                                Toast.makeText(Login.this, "conexion réussie réussie",Toast.LENGTH_SHORT).show();
                                //startActivity(new Intent(getApplicationContext(),DoctorMainActivity.class));
                                checkUserType(task.getU);

                            }else{
                                Toast.makeText(Login.this, "Mauvaises informations saisies",Toast.LENGTH_SHORT).show();
                            }

                        }




                });*/

                fauth.signInWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {

                        Toast.makeText(Login.this, "conexion réussie réussie",Toast.LENGTH_SHORT).show();
                        checkUserRole(authResult.getUser().getUid());

                    }
                });


            }
        });


        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Register.class));
            }
        });

    }

    private void checkUserRole(String uid) {

        DocumentReference dr = Fstore.collection("doctors").document(uid);

        dr.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                if(documentSnapshot.getString("isDoctor") != null){
                    startActivity(new Intent(getApplicationContext(),DoctorMainActivity.class));
                }

                if(documentSnapshot.getString("isIntervenant") != null){
                    startActivity(new Intent(getApplicationContext(),IntervenantMainActivity.class));
                }

                if(documentSnapshot.getString("isPatient") != null){
                    startActivity(new Intent(getApplicationContext(),Patient.class));
                }




            }
        });

    }
}
