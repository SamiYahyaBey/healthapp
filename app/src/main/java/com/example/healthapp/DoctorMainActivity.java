package com.example.healthapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;


public class DoctorMainActivity extends AppCompatActivity implements  SimpleDialogListener{
    private RecyclerView mRecyclerView;
    private MonRecyclerViewDoctor mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Button ajout_parcours;
    //  CoordinatorLayout mcoordinatorLayout;
    TextView dr_name;
    FirebaseAuth Fauth;
    FirebaseFirestore Fstore;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_doctors);

        //instance the firebase
        Fauth = FirebaseAuth.getInstance();
        Fstore = FirebaseFirestore.getInstance();
        userID = Fauth.getCurrentUser().getUid();
        DocumentReference dr = Fstore.collection("doctors").document(userID);
        dr.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable  DocumentSnapshot value, @Nullable  FirebaseFirestoreException error) {
                dr_name.setText(value.getString("nom"));
            }
        });

        dr_name = findViewById(R.id.dr_lastname);
        mRecyclerView = (RecyclerView) findViewById(R.id.list_doctors);
        mRecyclerView.setHasFixedSize(true);
        ajout_parcours = findViewById(R.id.ajouter_parcours);
        //listener pour afficher le formulaire
        ajout_parcours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForm();
            }
        });

        //métohe du onclick




        //   mcoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        mLayoutManager = new LinearLayoutManager(this);
        //  mLayoutManager=new GridLayoutManager(this,2,GridLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MonRecyclerViewDoctor(getDataSource());
        mRecyclerView.setAdapter(mAdapter);
        FirebaseDatabase database = FirebaseDatabase.getInstance("https://healthapp-bfa8d-default-rtdb.europe-west1.firebasedatabase.app/");
        DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Hello, World!");
        //RecyclerView.ItemDecoration itemDecoration =
        // new DividerItemDecoration(this, R.drawable.divider);
        //mRecyclerView.addItemDecoration(itemDecoration);
    }


    private void showForm() {

        FragmentManager fm = getSupportFragmentManager();
        AddParcoursFragement simpleDialogFragment = AddParcoursFragement.newInstance("Titre");
        simpleDialogFragment.show(fm, "fragment_simple_dialog");

    }


    private ArrayList<DonneeDoctor> getDataSource() {
        ArrayList results = new ArrayList<DonneeDoctor>();
        for (int index = 0; index < 20; index++) {
            Context mContext = getApplicationContext();
            DonneeDoctor obj = new DonneeDoctor("Patient " + index," DATE de naissance " +index,
                    mContext.getResources().getDrawable(R.drawable.avatarpatient));
            results.add(index,obj);
        }
        return results;
    }

    @Override
    public void onOkClickDialog(String inputText) {

    }


    public class DividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public DividerItemDecoration(Context context, int resId) {
            mDivider = ContextCompat.getDrawable(context, resId);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

}
